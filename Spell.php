<?php

/**
 * Basic spell definition
 * 
 * 
 * QUESTION: WHAT TO DO SPELLS THAT ARE MODIFIERS (RAPID FIRE, MIRROR IMAGE ETC>)
 * 
 */

 class Spell {

    private $name;
    private $spellid;
    private $baseDamageLow;
    private $baseDamageHigh;            
    private $scalingRatio;
    private $scalingStat;
    private $aoe;
    private $triggerGCD;

    public function __construct($name, $spellid, $baseDamageLow, $baseDamageHigh,
                         $scalingRatio, $scalingStat, $aoe){
        $this->name             = $name;
        $this->spellid          = $spellid;
        $this->baseDamageLow    = $baseDamageLow;
        $this->baseDamageHigh   = $baseDamageHigh;
        $this->scalingRatio     = $scalingRatio;
        $this->scalingStat      = $scalingStat;
        $this->aoe              = $aoe;
    }

    public function getName (){}
    public function getBaseDamageLow (){}
    public function getBaseDamageHigh (){}
    public function getScalingRatio (){}
    public function getScalingStat (){}
    public function getAoe (){}



 }